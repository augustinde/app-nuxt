FROM node:21-alpine3.18 as node-base
WORKDIR /app

FROM node-base as node-dev
ENV host=0.0.0.0
EXPOSE 3000
CMD ["npm", "run", "dev"]

FROM node-base as node-output
COPY /app /app
RUN npm install
RUN npm run build

FROM node-base as node-prod
ENV PORT=8080
COPY --from=node-output /app/.output /app
WORKDIR /app/server
CMD ["node", "index.mjs"]